This is my first project called "Silver Screen". It is a simple website about different movies and actors. Frameworks used:

1.  Node.js
2.  React.js

Hosted on Heroku. That is probably all :)

PS. Check out these pages, maybe you find something interesting

1.  Eleken's website - [https://www.eleken.co/](https://www.eleken.co/)
2.  SaaS Trends of 2021 Every Business Owner Should Know - [https://www.eleken.co/blog-posts/saas-trends](https://www.eleken.co/blog-posts/saas-trends)
3.  14 Essential UX Research Methods - [https://www.eleken.co/blog-posts/14-essential-ux-research-methods](https://www.eleken.co/blog-posts/14-essential-ux-research-methods)
4.  Product Designer Job Description - [https://www.eleken.co/blog-posts/product-designer-job-description-find-a-perfect-candidate-for-your-team](https://www.eleken.co/blog-posts/product-designer-job-description-find-a-perfect-candidate-for-your-team)
5.  Design Thinking Examples: Five Real Stories - [https://www.eleken.co/blog-posts/design-thinking-examples-five-real-stories](https://www.eleken.co/blog-posts/design-thinking-examples-five-real-stories)
6.  SaaS Onboarding: Educate, Engage and Retain Your Customers - [https://www.eleken.co/blog-posts/saas-onboarding-educate-engage-and-retain-your-customers](https://www.eleken.co/blog-posts/saas-onboarding-educate-engage-and-retain-your-customers)